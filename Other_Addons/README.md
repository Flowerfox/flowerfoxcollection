# My personal favorite legacy addons:

#### Tab Groups:
https://github.com/117649/Tab-Groups/releases

#### DownThemAll!:
https://github.com/xiaoxiaoflood/firefox-scripts/tree/master/extensions/DownThemAll

#### Save File to:
https://github.com/xiaoxiaoflood/firefox-scripts/tree/master/extensions/savefileto

#### Norwell:
https://github.com/xiaoxiaoflood/firefox-scripts/tree/master/extensions/norwellht

#

# My personal favorite WebExtensions addons:

#### uBlock Origin:
https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/

#### Redirector:
https://addons.mozilla.org/en-US/firefox/addon/redirector/

#### Decentraleyes:
https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/

#### Redirect AMP to HTML:
https://addons.mozilla.org/en-US/firefox/addon/amp2html/

#### Violentmonkey:
https://addons.mozilla.org/en-US/firefox/addon/violentmonkey/

#### Stylus:
https://addons.mozilla.org/en-US/firefox/addon/styl-us/

#### Dark Reader:
https://addons.mozilla.org/en-US/firefox/addon/darkreader/

#### Firefox Translations:
https://addons.mozilla.org/en-US/firefox/addon/firefox-translations/

#### Auto Tab Discard:
https://addons.mozilla.org/en-US/firefox/addon/auto-tab-discard/

#### Tab Stash:
https://addons.mozilla.org/en-US/firefox/addon/tab-stash/

#### New Tab Override:
https://addons.mozilla.org/en-US/firefox/addon/new-tab-override/

#### Side View
https://addons.mozilla.org/en-US/firefox/addon/side-view/

#### View Image in Context Menu:
https://addons.mozilla.org/en-US/firefox/addon/view-image-context-menu-item/

#### Activate Reader View:
https://addons.mozilla.org/en-US/firefox/addon/activate-reader-view/

#### NetEaseMusicWorld+:
(unblocks NetEase for international users)
https://addons.mozilla.org/en-US/firefox/addon/neteasemusicworldplus/

#### Divider:
(adds a single divider that you can use on the overflow menu. I recommend using [Space and Separator Restorer](https://gitlab.com/Flowerfox/flowerfoxcollection/-/tree/main/Ported_Addons/CustomJSforFx_Addons/Space_and_Separator_Restorer) for the toolbars)
https://addons.mozilla.org/en-US/firefox/addon/divider/

