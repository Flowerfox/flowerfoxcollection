# Flowerfox Quality of Life Tweaks

### This addon sets the following about:config flags:

##### (Universal)
Services.prefs.setIntPref("media.cache_readahead_limit", 99999); (Removes limit on pre-loading media)

Services.prefs.setIntPref("media.cache_resume_threshold", 99999); (See above)

Services.prefs.setBoolPref("signon.management.page.fileImport.enabled", true); (Allows you to import/export passwords in about:logins)

#

##### (Firefox-specific)
Services.prefs.setBoolPref("browser.newtabpage.activity-stream.showSponsored", false);

Services.prefs.setBoolPref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);

Services.prefs.setBoolPref("toolkit.legacyUserProfileCustomizations.stylesheets", true); (Enables userChrome.css)

#

Feel free to make suggestions!
The scope of this addon isn't just limited to about:config flags, so I'm open to any kinds of suggestions for mild usability improvements.
